package com.gs.gapp.osgi.generation.target

import com.gs.gapp.metamodel.java.JavaInterface
import com.gs.gapp.osgi.metamodel.DeclarativeServiceXml

class DeclarativeServiceXmlWriterSnippets {

	public static String getContent(DeclarativeServiceXml declarativeServiceXml) {
		
		// --- provided services
		StringBuilder providedServices = new StringBuilder();
		String newline = "";
		for (JavaInterface componentInterface : declarativeServiceXml.getProvidedInterfaces()) {
			providedServices.append(newline).append("        <provide interface=\"").append(componentInterface.getQualifiedName()).append("\"/>");
			newline = "\n";
		}
		
		// --- properties
		
		StringBuilder properties = new StringBuilder();
		newline = "";
		for (DeclarativeServiceXml.Property property : declarativeServiceXml.getProperties()) {
			
			properties.append(newline).append("""    <property name="${property.getName()}" type="${property.getType().getName()}" value="${property.getValue()}"/>""");
			newline = "\n";
		}
		
		// --- references
		StringBuilder references = new StringBuilder();
		newline = "";
		for (DeclarativeServiceXml.Reference reference : declarativeServiceXml.getReferences()) {
			
			references.append(newline).append("""    <reference bind="${reference.getBindMethod().getName()}" cardinality="${reference.getCardinality().toString()}" interface="${reference.getComponentInterface().getQualifiedName()}" name="${reference.getName()}" policy="${reference.getPolicy().toString().toLowerCase()}" unbind="${reference.getUnbindMethod().getName()}"/>""");
			newline = "\n";
		}
		
		String result =
"""<?xml version="1.0" encoding="UTF-8"?>

<scr:component xmlns:scr="http://www.osgi.org/xmlns/scr/v1.1.0"
    name="${declarativeServiceXml.getName()}"
    ${declarativeServiceXml.getActivationMethod() == null ? "" : ("activate=\"" + declarativeServiceXml.getActivationMethod().getName() + "\"")}
    ${declarativeServiceXml.getDeactivationMethod() == null ? "" : ("deactivate=\"" + declarativeServiceXml.getDeactivationMethod().getName() + "\"")}
    ${declarativeServiceXml.getModificationMethod() == null ? "" : ("modified=\"" + declarativeServiceXml.getModificationMethod().getName() + "\"")}
    ${declarativeServiceXml.getConfigurationPolicy() == null ? "" : ("configuration-policy=\"" + declarativeServiceXml.getConfigurationPolicy().toString() + "\"")}
    enabled="${declarativeServiceXml.isEnabled()}"
    immediate="${declarativeServiceXml.isImmediate()}">

    <implementation class="${declarativeServiceXml.getComponentClass().getQualifiedName()}"/>

${properties}

    <service>
${providedServices}
    </service>

${references}

</scr:component>
"""
        // regular expression taken from here: http://stackoverflow.com/questions/4123385/remove-all-empty-lines
        result = result.replaceAll("(?m)^[ \t]*\r?\n", "");
	}
}
