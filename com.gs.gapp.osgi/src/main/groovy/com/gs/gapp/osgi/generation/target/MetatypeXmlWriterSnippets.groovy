package com.gs.gapp.osgi.generation.target

import com.gs.gapp.osgi.metamodel.MetatypeXml
import com.gs.gapp.osgi.metamodel.MetatypeXml.AttributeDefinition
import com.gs.gapp.osgi.metamodel.MetatypeXml.Designate
import com.gs.gapp.osgi.metamodel.MetatypeXml.ObjectClassDefinition
import com.gs.gapp.osgi.metamodel.MetatypeXml.AttributeDefinition.AttributeOption

class MetatypeXmlWriterSnippets {

	public static String getContent(MetatypeXml metatypeXml) {
		
		StringBuilder ocds = new StringBuilder();
		for (ObjectClassDefinition objectClassDefinition : metatypeXml.getObjectClassDefinitions()) {
			ocds.append(getOCD(objectClassDefinition));
		}
		
		StringBuilder designates = new StringBuilder();
		for (Designate designate : metatypeXml.getDesignates()) {
			designates.append(getDesignate(designate));
		}
		
		String result =
"""<?xml version="1.0" encoding="UTF-8"?>
<MetaData xmlns="http://www.osgi.org/xmlns/metatype/v1.2.0" localization="en_us">
${ocds}
${designates}
</MetaData>
"""
	}
	
	/**
	 * @param ocd
	 * @return
	 */
	private static String getOCD(MetatypeXml.ObjectClassDefinition ocd) {
		
		StringBuilder attributeDefinitions = new StringBuilder();
		for (AttributeDefinition attributeDefinition : ocd.getAttributeDefinitions()) {
			attributeDefinitions.append(getAD(attributeDefinition));
		}
		
		String result = """    <OCD id="${ocd.getId()}"
        name="${ocd.getName()}"
        description="${ocd.getDescription()}">

        <Icon resource="${ocd.getIconResource()}" size="${ocd.getIconSize()}"/>

${attributeDefinitions}

    </OCD>"""
	}

	/**
	 * @param attributeDefinition
	 * @return
	 */
	private static String getAD(MetatypeXml.AttributeDefinition attributeDefinition) {
		StringBuilder attributeOptions = new StringBuilder();
		for (AttributeOption option : attributeDefinition.getAttributeOptions()) {
			attributeOptions.append("""\n            <Option label="${option.getLabel()}" value="${option.getValue()}"/>""");
		}
		
		String result = """        <AD id="${attributeDefinition.getId()}"
            name="${attributeDefinition.getName()}"
            type="${attributeDefinition.getType().getName()}"
            cardinality="${attributeDefinition.getCardinality()}"
            ${attributeDefinition.getMin() == null ? "" : "min=\""+attributeDefinition.getMin()+"\""}
            ${attributeDefinition.getMax() == null ? "" : "max=\""+attributeDefinition.getMax()+"\""}
            required="${attributeDefinition.isRequired()}"
            default="${attributeDefinition.getDefaultValue() == null ? '' : attributeDefinition.getDefaultValue()}"
            description="${attributeDefinition.getDescription() == null ? '' : attributeDefinition.getDescription()}">${attributeOptions}
        </AD>
"""
		// regular expression taken from here: http://stackoverflow.com/questions/4123385/remove-all-empty-lines
        result = result.replaceAll("(?m)^[ \t]*\r?\n", "");
	}
	
	/**
	 * @param designate
	 * @return
	 */
	private static String getDesignate(MetatypeXml.Designate designate) {
		String result = """    <Designate pid="${designate.getPid()}">
        <Object ocdref="${designate.getOcdref().getId()}"/>
    </Designate>
"""
	}
}
