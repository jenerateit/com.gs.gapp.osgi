package com.gs.gapp.osgi.converter;

import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.converter.basic.java.BasicToJavaConverterOptions;

/**
 * @author mmt
 *
 */
public class AbstractBasicToOsgiConverterOptions extends BasicToJavaConverterOptions {
	
	public AbstractBasicToOsgiConverterOptions(ModelConverterOptions options) {
		super(options);
	}
}
