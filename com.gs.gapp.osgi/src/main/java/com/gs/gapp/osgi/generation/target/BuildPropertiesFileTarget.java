/**
 * 
 */
package com.gs.gapp.osgi.generation.target;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetI;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.FindTargetScope;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.generation.java.target.PropertiesFileTarget;
import com.gs.gapp.osgi.metamodel.BuildPropertiesFile;
import com.gs.gapp.osgi.metamodel.DeclarativeServiceXml;

/**
 * @author mmt
 *
 */
public class BuildPropertiesFileTarget extends PropertiesFileTarget {

	@ModelElement
	private BuildPropertiesFile buildPropertiesFile;

	@Override
	public String getTargetPrefix() {
        return ".";
	}



	public static class BuildPropertiesFileWriter extends PropertiesFileWriter {
		
		@ModelElement
		private BuildPropertiesFile buildPropertiesFile;

		@Override
		public void transform(TargetSection ts) throws WriterException {

			// first prepare the entries in the properties file object and then call the super transform() in order to generate the model
			
			// --- output
			if (buildPropertiesFile.getOutput() != null && buildPropertiesFile.getOutput().length() > 0) {
				buildPropertiesFile.put("output..", buildPropertiesFile.getOutput());
			}
			
			// --- bin.includes
			String comma = "";
			String newline = "";
			String backslash = "";
			String indent = "";
			StringBuilder value = new StringBuilder();
			for (DeclarativeServiceXml declarativeServiceXml : buildPropertiesFile.getManifestFile().getDeclarativeServiceXmls()) {
				TargetI<? extends TargetDocumentI> declarativeServiceXmlTarget = findTarget(FindTargetScope.PROJECT, declarativeServiceXml, DeclarativeServiceXmlTarget.class, null);
				value.append(comma).append(backslash).append(newline).append(indent).append(declarativeServiceXmlTarget.getTargetPath().toString());
				comma = ",";
				backslash = "\\";
				newline = "\n";
				indent = "              ";
			}
			for (String binInclude : buildPropertiesFile.getBinIncludes()) {
				value.append(comma).append(backslash).append(newline).append(indent).append(binInclude);
				comma = ",";
				backslash = "\\";
				newline = "\n";
				indent = "              ";
			}
			if (value.length() > 0) buildPropertiesFile.put("bin.includes", value.toString());
			
			// --- source..
			comma = "";
			newline = "";
			backslash = "";
			indent = "";
			value = new StringBuilder();
			for (String sourcePath : buildPropertiesFile.getSourcePaths()) {
				value.append(comma).append(backslash).append(newline).append(indent).append(sourcePath);
				comma = ",";
				backslash = "\\";
				newline = "\n";
				indent = "           ";
			}
			if (value.length() > 0) buildPropertiesFile.put("source..", value.toString());
			
			
			super.transform(ts);
		}
	}
}
