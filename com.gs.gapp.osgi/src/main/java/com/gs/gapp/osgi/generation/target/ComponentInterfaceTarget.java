/**
 * 
 */
package com.gs.gapp.osgi.generation.target;

import org.jenerateit.annotation.ModelElement;

import com.gs.gapp.generation.java.target.JavaInterfaceTarget;
import com.gs.gapp.osgi.metamodel.ComponentInterface;

/**
 * @author mmt
 *
 */
public class ComponentInterfaceTarget extends JavaInterfaceTarget {

	@ModelElement
	private ComponentInterface componentInterface;

}
