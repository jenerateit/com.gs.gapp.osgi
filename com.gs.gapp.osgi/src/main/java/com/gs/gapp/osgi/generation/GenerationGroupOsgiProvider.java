/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */
package com.gs.gapp.osgi.generation;

import org.jenerateit.generationgroup.GenerationGroupConfigI;

import com.gs.gapp.generation.java.GenerationGroupJavaProvider;

/**
 * @author mmt
 *
 */
public class GenerationGroupOsgiProvider extends GenerationGroupJavaProvider {

	/**
	 * 
	 */
	public GenerationGroupOsgiProvider() {
		super();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.java.GenerationGroupProviderJava#getGenerationGroupConfig()
	 */
	@Override
	public GenerationGroupConfigI getGenerationGroupConfig() {
		return new GenerationGroupOsgi();
	}
}
