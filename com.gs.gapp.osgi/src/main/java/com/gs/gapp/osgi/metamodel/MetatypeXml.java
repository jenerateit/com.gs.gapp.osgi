package com.gs.gapp.osgi.metamodel;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.java.JavaTypeI;
import com.gs.gapp.metamodel.xml.XMLDocument;

public class MetatypeXml extends XMLDocument {

	private static final long serialVersionUID = -2049397243357159743L;
	
	private final Set<ObjectClassDefinition> objectClassDefinitions = new LinkedHashSet<>();
	
	private final Set<Designate> designates = new LinkedHashSet<>();

	/**
	 * @param name
	 */
	public MetatypeXml(String name) {
		super(name);
	}

	public Set<ObjectClassDefinition> getObjectClassDefinitions() {
		return objectClassDefinitions;
	}
	
	public boolean addObjectClassDefinition(ObjectClassDefinition objectClassDefinition) {
		return this.objectClassDefinitions.add(objectClassDefinition);
	}
	
	public String getFileName() {
		return new StringBuilder("OSGI-INF/metatype/").append(getName()).append(".xml").toString();
	}

	public Set<Designate> getDesignates() {
		return designates;
	}
	
	public boolean addDesignate(Designate designate) {
		return this.designates.add(designate);
	}

	/**
	 * @author mmt
	 *
	 */
	public static class AttributeDefinition extends ModelElement {

		private static final long serialVersionUID = 8964817910851826831L;
		
		private final String id;
		private final AttributeType type;

		private String description;
		private int cardinality;
		private String min = "";
		private String max = "";
		private String defaultValue = "";
		private boolean required = false;
		
		private final Set<AttributeOption> attributeOptions = new LinkedHashSet<>();
		
		public AttributeDefinition(String name, String id, AttributeType type) {
			super(name);
			this.id = id;
			this.type = type;
			
			this.description = new StringBuilder(name).append(" (").append(type.getName()).append(")").toString(); // use the name as the default description
		}
		
		
		

		/**
		 * @param name
		 * @param id
		 * @param type
		 * @param description
		 * @param cardinality
		 * @param min
		 * @param max
		 * @param defaultValue
		 * @param required
		 * @param attributeOptions
		 */
		public AttributeDefinition(String name, String id, AttributeType type, String description, int cardinality,
				String min, String max, String defaultValue, boolean required, AttributeOption... attributeOptions) {
			super(name);
			this.id = id;
			this.type = type;
			this.description = description;
			this.cardinality = cardinality;
			this.min = min;
			this.max = max;
			this.defaultValue = defaultValue;
			this.required = required;
			
			if (attributeOptions != null) this.attributeOptions.addAll(Arrays.asList(attributeOptions));
		}




		public String getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public AttributeType getType() {
			return type;
		}

		public int getCardinality() {
			return cardinality;
		}

		public void setCardinality(int cardinality) {
			this.cardinality = cardinality;
		}

		public String getMin() {
			return min;
		}

		public void setMin(String min) {
			this.min = min;
		}

		public String getMax() {
			return max;
		}

		public void setMax(String max) {
			this.max = max;
		}

		public String getDefaultValue() {
			return defaultValue;
		}

		public void setDefaultValue(String defaultValue) {
			this.defaultValue = defaultValue;
		}

		public boolean isRequired() {
			return required;
		}

		public void setRequired(boolean required) {
			this.required = required;
		}

		public Set<AttributeOption> getAttributeOptions() {
			return attributeOptions;
		}
		
		public boolean addAttributeOption(AttributeOption attributeOption) {
			return this.attributeOptions.add(attributeOption);
		}

		public static class AttributeOption extends ModelElement {

			private static final long serialVersionUID = 5928582107148675014L;
			
			private final String value;
			
			public AttributeOption(String label, String value) {
				super(label);
				this.value = value;
			}
			
			public String getLabel() {
				return getName();
			}

			public String getValue() {
				return value;
			}
		}
		
	}
	
	public static enum AttributeType {
		
		STRING ("String"),
		LONG ("Long"),
		DOUBLE ("Double"),
		FLOAT ("Float"),
		INTEGER ("Integer"),
		BYTE ("Byte"),
		CHAR ("Char"),
		BOOLEAN ("Boolean"),
		SHORT ("Short"),
		PASSWORD ("Password"),
		;
		
		private final String name;
		
		private AttributeType(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}
	
	/**
	 * @author mmt
	 *
	 */
	public static class ObjectClassDefinition extends ModelElement {
		
		private static final long serialVersionUID = -3882077660027231168L;
		
		private final String id;
		private String description;
		private String iconResource;
		private int iconSize;
		
		private final Set<AttributeDefinition> attributeDefinitions = new LinkedHashSet<>();
		
		/**
		 * @param name
		 */
		public ObjectClassDefinition(String name, String id) {
			super(name);
			this.id = id;
		}
		
		/**
		 * @param javaType
		 */
		public ObjectClassDefinition(JavaTypeI javaType) {
			super(javaType.getQualifiedName());
			this.id = javaType.getQualifiedName();
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getId() {
			return id;
		}
		
		public Set<AttributeDefinition> getAttributeDefinitions() {
			return attributeDefinitions;
		}
		
		public boolean addAttributeDefinition(AttributeDefinition attributeDefinition) {
			return this.attributeDefinitions.add(attributeDefinition);
		}

		public String getIconResource() {
			return iconResource;
		}

		public void setIconResource(String iconResource) {
			this.iconResource = iconResource;
		}

		public int getIconSize() {
			return iconSize;
		}

		public void setIconSize(int iconSize) {
			this.iconSize = iconSize;
		}
	}
	
	/**
	 * @author mmt
	 *
	 */
	public static class Designate extends ModelElement {
		
		private static final long serialVersionUID = 256129342449645262L;
		
		private ObjectClassDefinition ocdref;
		
		/**
		 * @param name
		 */
		public Designate(String name, ObjectClassDefinition ocdref) {
			super(name);
			this.setOcdref(ocdref);
		}

		public String getPid() {
			return getName();
		}

		public ObjectClassDefinition getOcdref() {
			return ocdref;
		}

		public void setOcdref(ObjectClassDefinition ocdref) {
			this.ocdref = ocdref;
		}
		
	}
}
