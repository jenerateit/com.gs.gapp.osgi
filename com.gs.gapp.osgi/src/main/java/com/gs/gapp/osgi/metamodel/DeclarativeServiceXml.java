package com.gs.gapp.osgi.metamodel;

import java.util.LinkedHashSet;
import java.util.Set;

import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.java.JavaInterface;
import com.gs.gapp.metamodel.java.JavaMethod;
import com.gs.gapp.metamodel.java.JavaTypeI;
import com.gs.gapp.metamodel.xml.XMLDocument;

public class DeclarativeServiceXml extends XMLDocument {

	private static final long serialVersionUID = -2049397243357159743L;
	
	private ComponentClass componentClass;
	
	private boolean enabled = true;
	private boolean immediate = true;
	private ConfigurationPolicy configurationPolicy = ConfigurationPolicy.OPTIONAL;
	
	private JavaMethod activationMethod;
	private JavaMethod deactivationMethod;
	private JavaMethod modificationMethod;
	
	private final Set<Reference> references = new LinkedHashSet<>();
	
	private final Set<Property> properties = new LinkedHashSet<>();

	/**
	 * @param name
	 */
	public DeclarativeServiceXml(String name) {
		super(name);
	}
	
	

	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}



	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}



	/**
	 * @return the immediate
	 */
	public boolean isImmediate() {
		return immediate;
	}



	/**
	 * @param immediate the immediate to set
	 */
	public void setImmediate(boolean immediate) {
		this.immediate = immediate;
	}



	/**
	 * @return the activationMethod
	 */
	public JavaMethod getActivationMethod() {
		return activationMethod;
	}



	/**
	 * @param activationMethod the activationMethod to set
	 */
	public void setActivationMethod(JavaMethod activationMethod) {
		this.activationMethod = activationMethod;
	}



	/**
	 * @return the deactivationMethod
	 */
	public JavaMethod getDeactivationMethod() {
		return deactivationMethod;
	}



	/**
	 * @param deactivationMethod the deactivationMethod to set
	 */
	public void setDeactivationMethod(JavaMethod deactivationMethod) {
		this.deactivationMethod = deactivationMethod;
	}



	/**
	 * @return the modificationMethod
	 */
	public JavaMethod getModificationMethod() {
		return modificationMethod;
	}



	/**
	 * @param modificationMethod the modificationMethod to set
	 */
	public void setModificationMethod(JavaMethod modificationMethod) {
		this.modificationMethod = modificationMethod;
	}



	public ComponentClass getComponentClass() {
		return componentClass;
	}

	public void setComponentClass(ComponentClass javaImplementationClass) {
		this.componentClass = javaImplementationClass;
	}
	
	public String getFileName() {
		return new StringBuilder("OSGI-INF/").append(getName()).append(".xml").toString();
	}
	
	public Set<JavaInterface> getProvidedInterfaces() {
		return componentClass.getComponentInterfaces();
	}
	
	public Set<Reference> getReferences() {
		return references;
	}
	
	public boolean addReference(Reference reference) {
		return this.references.add(reference);
	}

	public Set<Property> getProperties() {
		return properties;
	}

	public boolean addProperty(Property property) {
		return this.properties.add(property);
	}
	
	public ConfigurationPolicy getConfigurationPolicy() {
		return configurationPolicy;
	}



	public void setConfigurationPolicy(ConfigurationPolicy configurationPolicy) {
		this.configurationPolicy = configurationPolicy;
	}

	/**
	 * @author mmt
	 *
	 */
	public static class Reference extends ModelElement {

		private static final long serialVersionUID = 3392180858969836656L;
		
		private JavaMethod bindMethod;
		private JavaMethod unbindMethod;
		private ReferencePolicy policy = ReferencePolicy.STATIC;
		private ReferenceCardinality cardinality = ReferenceCardinality.MANDATORY;
		private JavaTypeI componentInterface;

		public Reference(String name) {
			super(name);
		}

		public JavaMethod getBindMethod() {
			return bindMethod;
		}

		public void setBindMethod(JavaMethod bindMethod) {
			this.bindMethod = bindMethod;
		}

		public JavaMethod getUnbindMethod() {
			return unbindMethod;
		}

		public void setUnbindMethod(JavaMethod unbindMethod) {
			this.unbindMethod = unbindMethod;
		}

		public ReferencePolicy getPolicy() {
			return policy;
		}

		public void setPolicy(ReferencePolicy policy) {
			this.policy = policy;
		}

		public ReferenceCardinality getCardinality() {
			return cardinality;
		}

		public void setCardinality(ReferenceCardinality cardinality) {
			this.cardinality = cardinality;
		}

		public JavaTypeI getComponentInterface() {
			return componentInterface;
		}

		public void setComponentInterface(JavaTypeI componentInterface) {
			this.componentInterface = componentInterface;
		}
	}
	
	/**
	 * @author mmt
	 *
	 */
	public static class Property extends ModelElement {

		private static final long serialVersionUID = 8964817910851826831L;
		
		private final PropertyType type;

		private final String value;
		
		
		public Property(String name, String value, PropertyType type) {
			super(name);
			this.value = value;
			this.type = type;
		}


		/**
		 * @return the type
		 */
		public PropertyType getType() {
			return type;
		}


		/**
		 * @return the value
		 */
		public String getValue() {
			return value;
		}
	}
	
	public static enum PropertyType {
		
		STRING ("String"),
		LONG ("Long"),
		DOUBLE ("Double"),
		FLOAT ("Float"),
		INTEGER ("Integer"),
		BYTE ("Byte"),
		CHARACTER ("Character"),
		BOOLEAN ("Boolean"),
		SHORT ("Short"),
		;
		
		private final String name;
		
		private PropertyType(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}
}
