package com.gs.gapp.osgi.generation.target;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.stream.Collectors;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.AbstractTextTargetDocument;
import org.jenerateit.target.TargetException;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractTextWriter;
import org.osgi.framework.VersionRange;

import com.gs.gapp.generation.basic.target.BasicTextTarget;
import com.gs.gapp.metamodel.java.JavaPackage;
import com.gs.gapp.osgi.generation.GenerationGroupOsgiOptions;
import com.gs.gapp.osgi.generation.target.ManifestFileTarget.ManifestFileTargetDocument;
import com.gs.gapp.osgi.metamodel.ManifestFile;



//@OneOff
public class ManifestFileTarget extends BasicTextTarget<ManifestFileTargetDocument> {

	@ModelElement
	private ManifestFile manifestFile;
	
	@Override
	public URI getTargetURI() {
		try {
			return new URI("META-INF/MANIFEST.MF");
		} catch (URISyntaxException e) {
			throw createTargetException(e, this, manifestFile);
		}
	}
	
	public static class ManifestFileWriter extends AbstractTextWriter {
		
		@ModelElement
		private ManifestFile manifestFile;

		private GenerationGroupOsgiOptions options;
		
		private static final String BUNDLE_MANIFEST_VERSION = "Bundle-ManifestVersion";
		private static final String MANIFEST_VERSION = "Manifest-Version";
		private static final String BUNDLE_NAME = "Bundle-Name";
		private static final String BUNDLE_SYMBOLIC_NAME = "Bundle-SymbolicName";
		private static final String BUNDLE_VERSION = "Bundle-Version";
		private static final String BUNDLE_CLASS_PATH = "Bundle-ClassPath";
		private static final String BUNDLE_VENDOR = "Bundle-Vendor";
		private static final String BUNDLE_ACTIVATION_POLICY = "Bundle-ActivationPolicy";
		private static final String FRAGMENT_HOST = "Fragment-Host";
		private static final String BUNDLE_REQ_EXEC_ENV = "Bundle-RequiredExecutionEnvironment";
		private static final String SERVICE_COMPONENT = "Service-Component";
		private static final String IMPORT_PACKAGE = "Import-Package";
		private static final String EXPORT_PACKAGE = "Export-Package";
		private static final String REQUIRE_BUNDLE = "Require-Bundle";
		private static final String BUNDLE_NATIVE_CODE = "Bundle-NativeCode";
		
		@Override
		public void transform(TargetSection ts) {
			boolean overwrite = generationGroupOsgiOptions().isGenerationManifestOverwrite();
			
			ManifestFileTargetDocument previousTargetDocument = ManifestFileTarget.class.cast(getTextTransformationTarget()).getPreviousTargetDocument();

			/*
			 * compare each line of manifest with old entry.
			 * Overwrite with generation result if overwrite is true, or nor entry is
			 * found in the previous manifest.
			 * then remove the entry from the old document.
			 */
			Attributes attributes = previousTargetDocument.getManifest() == null ? new Attributes(0) : previousTargetDocument.getManifest().getMainAttributes();
			if (overwrite || attributes.getValue(MANIFEST_VERSION) == null) {
				wNL(MANIFEST_VERSION, ": 1.0");
			} else {
				wNL(MANIFEST_VERSION, ": ", attributes.getValue(MANIFEST_VERSION));
			}
			remove(attributes, MANIFEST_VERSION);
			
			if (overwrite || attributes.getValue(BUNDLE_MANIFEST_VERSION) == null) {
				wNL(BUNDLE_MANIFEST_VERSION, ": 2");
			} else {
				wNL(BUNDLE_MANIFEST_VERSION, ": ", attributes.getValue(BUNDLE_MANIFEST_VERSION));
			}
			remove(attributes, BUNDLE_MANIFEST_VERSION);
			
			if (overwrite || attributes.getValue(BUNDLE_NAME) == null) {
				wNL(BUNDLE_NAME, ": ", manifestFile.getBundleName() == null ? "" :  manifestFile.getBundleName());
			} else {
				wNL(BUNDLE_NAME, ": ", attributes.getValue(BUNDLE_NAME));
			}
			remove(attributes, BUNDLE_NAME);
			
			if (overwrite || attributes.getValue(BUNDLE_SYMBOLIC_NAME) == null) {
				wNL(BUNDLE_SYMBOLIC_NAME, ": ", manifestFile.getBundleSymbolicName(), ";singleton:=", Boolean.toString(manifestFile.isSingleton()));
			} else {
				wNL(BUNDLE_SYMBOLIC_NAME, ": ", attributes.getValue(BUNDLE_SYMBOLIC_NAME));
			}
			remove(attributes, BUNDLE_SYMBOLIC_NAME);

			if (overwrite || attributes.getValue(BUNDLE_VERSION) == null) {
				wNL(BUNDLE_VERSION, ": ", manifestFile.getBundleVersion().toString());
			} else {
				wNL(BUNDLE_VERSION, ": ", attributes.getValue(BUNDLE_VERSION));
			}
			remove(attributes, BUNDLE_VERSION);
			
			/* add the bundle classpath from the metamodel to the existing entries and sort them */
			if (!manifestFile.getBundleClasspath().isEmpty() || attributes.getValue(BUNDLE_CLASS_PATH) != null) {
				SortedSet<String> bundleClassPath = new TreeSet<>(manifestFile.getBundleClasspath());
				if (attributes.getValue(BUNDLE_CLASS_PATH) != null) {
					bundleClassPath.addAll(Arrays.stream(attributes.getValue(BUNDLE_CLASS_PATH).split(","))
							.map(String::trim)
							.collect(Collectors.toSet()));
				}
				wNL(BUNDLE_CLASS_PATH, ": ", String.join(",", bundleClassPath));
				remove(attributes, BUNDLE_CLASS_PATH);
			}

			if (overwrite || attributes.getValue(BUNDLE_VENDOR) == null) {
				wNL(BUNDLE_VENDOR, ": ", manifestFile.getBundleVendor() == null ? "" : manifestFile.getBundleVendor());
			} else {
				wNL(BUNDLE_VENDOR, ": ", attributes.getValue(BUNDLE_VENDOR));
			}
			remove(attributes, BUNDLE_VENDOR);
			
			if (manifestFile.isLazyBundleActivationPolicy() || attributes.getValue(BUNDLE_ACTIVATION_POLICY) != null) {
				if (overwrite || attributes.getValue(BUNDLE_ACTIVATION_POLICY) == null) {
					wNL(BUNDLE_ACTIVATION_POLICY, ": lazy");
				} else {
					wNL(BUNDLE_ACTIVATION_POLICY, ": ", attributes.getValue(BUNDLE_ACTIVATION_POLICY));
				}
				remove(attributes, BUNDLE_ACTIVATION_POLICY);
			}
		
			if (manifestFile.getFragmentHost() != null || attributes.getValue(FRAGMENT_HOST) != null) {
				if (overwrite || attributes.getValue(FRAGMENT_HOST) == null) {
					if (manifestFile.getFragmentHost() != null) {
						wNL(FRAGMENT_HOST, ": ", manifestFile.getFragmentHost(), ";bundle-version=\"", manifestFile.getFragmentHostBundleVersion(), "\"");
					}
				} else {
					wNL(FRAGMENT_HOST, ": ", attributes.getValue(FRAGMENT_HOST));
				}
				remove(attributes, FRAGMENT_HOST);
			}
			
			if (manifestFile.getRequiredExecutionEnvironment() != null || attributes.getValue(BUNDLE_REQ_EXEC_ENV) != null) {
				if (overwrite || attributes.getValue(BUNDLE_REQ_EXEC_ENV) == null) {
					if (manifestFile.getRequiredExecutionEnvironment() != null) {
						wNL(BUNDLE_REQ_EXEC_ENV, ": ", manifestFile.getRequiredExecutionEnvironment().getName());
					}
				} else {
					wNL(BUNDLE_REQ_EXEC_ENV, ": ", attributes.getValue(BUNDLE_REQ_EXEC_ENV));
				}
				remove(attributes, BUNDLE_REQ_EXEC_ENV);
			}
			
			if (!manifestFile.getDeclarativeServiceXmls().isEmpty() || attributes.getValue(SERVICE_COMPONENT) != null) {
				SortedSet<String> services = new TreeSet<>(manifestFile.getDeclarativeServiceXmls().stream()
						.map(s -> s.getFileName().trim())
						.collect(Collectors.toSet()));
				if (attributes.getValue(SERVICE_COMPONENT) != null) {
					services.addAll(Arrays.stream(attributes.getValue(SERVICE_COMPONENT).split(","))
							.map(String::trim)
							.collect(Collectors.toSet()));
				}
				wNL(SERVICE_COMPONENT, ": ", String.join("\n ", services));
				remove(attributes, SERVICE_COMPONENT);
			}
			
			/* 
			 * Add the imports from the metamodel to the manifest, combine them with the existing entries.
			 * If overwrite is true the entries from the metamodel will overwrite existing versions.
			 */
			if (!manifestFile.getImportedPackages().isEmpty() || !manifestFile.getImportedPackagesAsStrings().isEmpty() || attributes.getValue(IMPORT_PACKAGE) != null) {
				SortedMap<String, VersionRange> imports = new TreeMap<>(manifestFile.getImportedPackagesAsStrings());
				for (Map.Entry<JavaPackage, VersionRange> imprt : manifestFile.getImportedPackages().entrySet()) {
					imports.put(imprt.getKey().getQualifiedName("."), imprt.getValue());
				}
				
				if (attributes.getValue(IMPORT_PACKAGE) != null) {
					mergeAndExtractImportExportPakages(imports, attributes.getValue(IMPORT_PACKAGE), overwrite);
				}
				
				wNL(IMPORT_PACKAGE, ": ", joinImportExports(imports));
				remove(attributes, IMPORT_PACKAGE);
			}
			
			if (!manifestFile.getExportedPackages().isEmpty() || attributes.getValue(EXPORT_PACKAGE) != null) {
				SortedMap<String, VersionRange> exports = new TreeMap<>();
				for (Map.Entry<JavaPackage, VersionRange> expo : manifestFile.getExportedPackages().entrySet()) {
					exports.put(expo.getKey().getQualifiedName("."), expo.getValue());
				}
				
				if (attributes.getValue(EXPORT_PACKAGE) != null) {
					mergeAndExtractImportExportPakages(exports, attributes.getValue(EXPORT_PACKAGE), overwrite);
				}
				
				wNL(EXPORT_PACKAGE, ": ", joinImportExports(exports));
				remove(attributes, EXPORT_PACKAGE);
			}

			if (!manifestFile.getRequireBundles().isEmpty() || attributes.getValue(REQUIRE_BUNDLE) != null) {
				SortedMap<String, VersionRange> requires = new TreeMap<>(manifestFile.getRequireBundles());
				
				if (attributes.getValue(REQUIRE_BUNDLE) != null) {
					mergeAndExtractImportExportPakages(requires, attributes.getValue(REQUIRE_BUNDLE), overwrite);
				}
				
				wNL(REQUIRE_BUNDLE, ": ", joinImportExports(requires));
				remove(attributes, REQUIRE_BUNDLE);
			}
			
			if (!manifestFile.getNativeCodes().isEmpty() || attributes.getValue(BUNDLE_NATIVE_CODE) != null) {
				if (overwrite || attributes.getValue(BUNDLE_NATIVE_CODE) == null) {
					wNL(manifestFile.getNativeCodesAsCommaSeparatedList());
				} else {
					wNL(BUNDLE_NATIVE_CODE, ": ", attributes.getValue(BUNDLE_NATIVE_CODE));
				}
				remove(attributes, BUNDLE_NATIVE_CODE);
			}
			
			for (Map.Entry<String, String> customEntry : manifestFile.getCustomEntries().entrySet()) {
				if (overwrite || attributes.getValue(customEntry.getKey()) == null) {
					wNL(customEntry.getKey(), ": ", customEntry.getValue());
				} else {
					wNL(customEntry.getKey(), ": ", attributes.getValue(customEntry.getKey()));
				}
				remove(attributes, customEntry.getKey());
			}
			
			for (Map.Entry<Object, Object> entry : attributes.entrySet()) {
				wNL(Attributes.Name.class.cast(entry.getKey()).toString(), ": ", String.class.cast(entry.getValue()));
			}
			
			/* last empty line in manifest */
			wNL();
			
		}
		
		private static void remove(Attributes attributes, String key) {
			attributes.remove(new Attributes.Name(key));
		}
		
		private static void mergeAndExtractImportExportPakages(SortedMap<String, VersionRange> importExports, String commaSeparatedList, boolean overwrite) {
			/* 
			 * splitting comma, but not commas in quotes
			 * https://stackoverflow.com/a/18893443
			 */
			for (String imprt : commaSeparatedList.split(",(?=(?:[^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)")) {
				String[] split = imprt.split(";");
				String pckg = split[0];
				VersionRange versionRange = null;
				for (String s : split) {
					if (s.contains("version=")) {
						versionRange = new VersionRange(s.replace("version=\"", "").replace("\"", ""));
					}
				}
				if (!overwrite || !importExports.containsKey(pckg)) {
					importExports.put(pckg, versionRange);
				}
			}
		}
		
		private static String joinImportExports(SortedMap<String, VersionRange> importExports) {
			return importExports.entrySet().stream()
					.map(i -> i.getKey() + (i.getValue() == null ? "" : ";version=\"" + i.getValue().toString() + "\""))
					.collect(Collectors.joining(",\n "));
		}
		
		private GenerationGroupOsgiOptions generationGroupOsgiOptions() {
			if (options == null) {
				options = new GenerationGroupOsgiOptions(this);
			}
			return options;
		}
	}

    public static class ManifestFileTargetDocument extends AbstractTextTargetDocument {
		
		private static final long serialVersionUID = 1L;

		public static final TargetSection DOCUMENT = new TargetSection("document", 10);

		private static final SortedSet<TargetSection> SECTIONS = new TreeSet<TargetSection>(
				Arrays.asList(new TargetSection[] {
						DOCUMENT
				}));

		private Manifest manifest;

		public Manifest getManifest() {
			return manifest;
		}

		@Override
		protected void analyzeDocument() {
			super.analyzeDocument();
			
			try {
				this.manifest = new Manifest(new ByteArrayInputStream(toByte()));
			} catch (IOException e) {
				e.printStackTrace();
				throw new TargetException("failed to parse old Manifest", e);
			}
		}
		
		/* (non-Javadoc)
		 * @see org.jenerateit.target.TextTargetDocumentI#getCommentEnd()
		 */
		@Override
		public CharSequence getCommentEnd() {
			return null;
		}

		/* (non-Javadoc)
		 * @see org.jenerateit.target.TextTargetDocumentI#getCommentStart()
		 */
		@Override
		public CharSequence getCommentStart() {
			return "X-COMMENT,";
		}

		/* (non-Javadoc)
		 * @see org.jenerateit.target.TextTargetDocumentI#getPrefixChar()
		 */
		@Override
		public char getPrefixChar() {
			return 0;
		}

		/* (non-Javadoc)
		 * @see org.jenerateit.target.TargetDocumentI#getTargetSections()
		 */
		@Override
		public SortedSet<TargetSection> getTargetSections() {
			return SECTIONS;
		}
	}
}
