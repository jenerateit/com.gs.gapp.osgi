package com.gs.gapp.osgi.generation;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.jenerateit.writer.AbstractWriter;

import com.gs.gapp.generation.basic.target.BasicTextTarget;
import com.gs.gapp.generation.java.GenerationGroupJavaOptions;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionBoolean;

/**
 * @author mmt
 *
 */
public class GenerationGroupOsgiOptions extends GenerationGroupJavaOptions {

	/**
	 * This option can be true or false. If it is set to true, Manifest entries will be
	 * overwritten by the generation.
	 */
	public static final OptionDefinitionBoolean OPTION_DEF_ENABLE_MANIFEST_OVERWRITE =
			new OptionDefinitionBoolean("enable-manifest-overwrite",
					                    "'true' manifest entries will be overwritten by the generation");

	
	public GenerationGroupOsgiOptions(AbstractWriter writer) {
		super(writer);
	}
	
	public GenerationGroupOsgiOptions(BasicTextTarget<?> target) {
		super(target);
	}

	/**
	 * This option can be true or false. If it is set to true, Manifest entries will be
	 * overwritten by the generation.
	 */
	public boolean isGenerationManifestOverwrite() {
		Serializable option = getOptionValue(OptionDefinitionEnum.OPTION_ENABLE_MANIFEST_OVERWRITE.getName());
		validateBooleanOption(option, OptionDefinitionEnum.OPTION_ENABLE_MANIFEST_OVERWRITE.getName());
		return option == null ? false : Boolean.parseBoolean(option.toString());
	}
	
	
	@Override
	public Set<OptionDefinition<? extends Serializable>> getOptionDefinitions() {
		Set<OptionDefinition<? extends Serializable>> optionDefinitions = super.getOptionDefinitions();
		optionDefinitions.addAll(OptionDefinitionEnum.getDefinitions());
		return optionDefinitions;
	}
	
	public static enum OptionDefinitionEnum {

		/**
		 * This option can be true or false. If it is set to true, Manifest entries will be
		 * overwritten by the generation.
		 */
		OPTION_ENABLE_MANIFEST_OVERWRITE ( OPTION_DEF_ENABLE_MANIFEST_OVERWRITE ),
		
		;

		private static final Map<String, OptionDefinitionEnum> stringToEnum = new HashMap<>();

		static {
			for (OptionDefinitionEnum m : values()) {
				stringToEnum.put(m.getName(), m);
			}
		}
		
		/**
		 * @return
		 */
		public static Set<OptionDefinition<? extends Serializable>> getDefinitions() {
			Set<OptionDefinition<? extends Serializable>> result = new LinkedHashSet<>();
			for (OptionDefinitionEnum m : values()) {
				result.add(m.getDefinition());
			}
			return result;
		}

		/**
		 * @param datatypeName
		 * @return
		 */
		public static OptionDefinitionEnum fromString(String datatypeName) {
			return stringToEnum.get(datatypeName);
		}

		private final OptionDefinition<? extends Serializable> definition;
		
		private OptionDefinitionEnum(OptionDefinition<? extends Serializable> definition) {
			this.definition = definition;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return this.getDefinition().getName();
		}

		public OptionDefinition<? extends Serializable> getDefinition() {
			return definition;
		}
	}
}
