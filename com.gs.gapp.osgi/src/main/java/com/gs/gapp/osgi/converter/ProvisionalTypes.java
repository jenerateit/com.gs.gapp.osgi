package com.gs.gapp.osgi.converter;

import com.gs.gapp.metamodel.java.JavaClass;
import com.gs.gapp.metamodel.java.JavaPackage;

public class ProvisionalTypes {

	// TODO remove this class
	//
	// these classes are still missing in predef (4-Dec-18 fra)
	//
	
	private static final JavaClass componentContext;
	private static final JavaClass logger;
	private static final JavaClass loggerFactory;
	
	
	static {
		componentContext = new JavaClass("ComponentContext", new JavaPackage("component", new JavaPackage("service", new JavaPackage("osgi", new JavaPackage("org", null)))));
		
		JavaPackage slf4j = new JavaPackage("sl4fj", new JavaPackage("org", null));
		logger = new JavaClass("Logger", slf4j);
		loggerFactory = new JavaClass("LoggerFactory", slf4j);
	}
	
	public static JavaClass getComponentcontext() {
		return componentContext;
	}
	
	public static JavaClass getLogger() {
		return logger;
	}
	
	public static JavaClass getLoggerfactory() {
		return loggerFactory;
	}
}
