/**
 *
 */
package com.gs.gapp.osgi.converter;

import java.util.List;

import com.gs.gapp.converter.basic.java.BasicToJavaConverter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.osgi.metamodel.ComponentClass;
import com.gs.gapp.osgi.metamodel.DeclarativeServiceXml;

/**
 * @author mmt
 *
 */
public abstract class AbstractBasicToOsgiConverter extends BasicToJavaConverter {

	public AbstractBasicToOsgiConverter() {
		super();
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.BasicConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();
		
		result.add( new ComponentClassToDeclarativeServiceXmlConverter<ComponentClass, DeclarativeServiceXml>(this) );
		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.function.java.FunctionToJavaConverter#getConverterOptions()
	 */
	@Override
	public AbstractBasicToOsgiConverterOptions getConverterOptions() {
		return null;
	}

	protected String getAdditionalSubpackageName() {
		return null;
	}
}
