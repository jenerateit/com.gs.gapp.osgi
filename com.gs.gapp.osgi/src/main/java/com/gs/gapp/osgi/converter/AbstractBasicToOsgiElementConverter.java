/**
 * 
 */
package com.gs.gapp.osgi.converter;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.library.converter.JavaModelElementCreator;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Version;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.java.JavaClass;
import com.gs.gapp.metamodel.java.JavaField;
import com.gs.gapp.metamodel.java.JavaMethod;
import com.gs.gapp.metamodel.java.JavaModifier;
import com.gs.gapp.metamodel.java.JavaPackage;
import com.gs.gapp.metamodel.java.JavaTypeI;
import com.gs.gapp.metamodel.java.generics.JavaParameterizedType;
import com.gs.gapp.osgi.metamodel.ComponentClass;
import com.gs.gapp.osgi.metamodel.ComponentClass.ComponentClassFieldEnum;
import com.gs.gapp.osgi.metamodel.ComponentClass.ComponentClassMethodEnum;
import com.gs.gapp.osgi.metamodel.DeclarativeServiceXml;
import com.gs.gapp.osgi.metamodel.ManifestFile;
import com.gs.gapp.osgi.metamodel.ManifestFile.RequiredExecutionEnironment;

/**
 * @author mmt
 *
 */
public abstract class AbstractBasicToOsgiElementConverter<S extends ModelElementI, T extends ModelElementI> extends AbstractM2MModelElementConverter<S, T> {

	public AbstractBasicToOsgiElementConverter(AbstractConverter modelConverter, ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}
	
	@Override
	protected AbstractBasicToOsgiConverter getModelConverter() {
		return (AbstractBasicToOsgiConverter) super.getModelConverter();
	}
	
	/**
	 * @param javaPackage
	 * @param nameOfSubpackage
	 * @return
	 */
	public JavaPackage getOrCreateSubpackage(JavaPackage javaPackage, String nameOfSubpackage) {
        JavaPackage result = null;
		
		if (nameOfSubpackage != null) {
				
			for (JavaPackage ownedPackage : javaPackage.getOwnedPackages()) {
				if (ownedPackage.getName().equals(nameOfSubpackage)) {
					result = ownedPackage;
					break;
				}
			}
			
			if (result == null) {
				result = new JavaPackage(nameOfSubpackage, javaPackage);
				addModelElement(result, javaPackage.getQualifiedName("."));
			}
		} else {
			result = javaPackage;
		}
		
		
		String additionalSubpackageName = getModelConverter().getAdditionalSubpackageName();
		if (additionalSubpackageName != null && additionalSubpackageName.equals(nameOfSubpackage) == false) {
			result = getOrCreateSubpackage(result, additionalSubpackageName);
		}
		
		return result;
	}
	
	
	
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);
		
		if (resultingModelElement instanceof ComponentClass) {
			
			// --- finding and constructing required types
			ComponentClass componentClass = (ComponentClass) resultingModelElement;
			getJavaCreator().createPublicNoArgsConstructor(componentClass);  // every component is provided with a no-args constructor
			JavaTypeI componentContextType = ProvisionalTypes.getComponentcontext();
			JavaTypeI bundleContextType = vd.predef.org.osgi.framework.BundleContext.getType();
			JavaParameterizedType parameterizedMapType = 
					vd.predef.java.util.Map.getParameterizedType(vd.predef.java.lang.String.getType(), vd.predef.java.lang.Object.getType());
			JavaTypeI integerType = vd.predef.java.lang.Integer.getType(); 
			
			// --- fields
			for (ComponentClassFieldEnum componentClassFieldEnum : ComponentClassFieldEnum.values()) {
				switch (componentClassFieldEnum) {
				case PROPERTIES:
					JavaField propertiesField = getJavaCreator().createPrivateField(parameterizedMapType, componentClassFieldEnum.getName(), componentClass);
					propertiesField.setBody(componentClassFieldEnum.getDocumentation());
					componentClass.setPropertiesField(propertiesField);
					break;
				default:
					throw new ModelConverterException("unhandled enum entry found: '" + componentClassFieldEnum + "'");
				}
			}
			
			// --- methods
			for (ComponentClassMethodEnum componentClassMethodEnum : ComponentClassMethodEnum.values()) {
				switch (componentClassMethodEnum) {
				case ACTIVATE:
					@SuppressWarnings("unused")
					JavaMethod activateMethod = getJavaCreator().createProtectedMethod(null, componentClassMethodEnum.getName(), componentClass,
							new JavaModelElementCreator.ParamInfo(componentContextType, "componentContext"),
							new JavaModelElementCreator.ParamInfo(bundleContextType, "bundleContext"),
							new JavaModelElementCreator.ParamInfo(parameterizedMapType, "properties"));
					break;
				case DEACTIVATE:
					@SuppressWarnings("unused")
					JavaMethod deactivateMethod = getJavaCreator().createProtectedMethod(null, componentClassMethodEnum.getName(), componentClass,
							new JavaModelElementCreator.ParamInfo(integerType, "reason"),
							new JavaModelElementCreator.ParamInfo(componentContextType, "componentContext"),
							new JavaModelElementCreator.ParamInfo(bundleContextType, "bundleContext"),
							new JavaModelElementCreator.ParamInfo(parameterizedMapType, "properties"));
					break;
				case MODIFIED:
					@SuppressWarnings("unused")
					JavaMethod modifiedMethod = getJavaCreator().createProtectedMethod(null, componentClassMethodEnum.getName(), componentClass,
							new JavaModelElementCreator.ParamInfo(componentContextType, "componentContext"));
					break;
				default:
					throw new ModelConverterException("unhandled component class method enum entry found: '" + componentClassMethodEnum + "'");
				}
			}
			
			if ( (componentClass.getModifier() & JavaModifier.ABSTRACT) > 0) {
				// nothing to be done here for an abstract class
			} else {
				if (componentClass.getManifestFile() == null) throw new NullPointerException("Component class '" + componentClass + "' does not have a manifest file set. Set the component class' manifest file in the onCreate() method that creates the component class.");
				DeclarativeServiceXml declarativeServiceXml = convertWithOtherConverter(DeclarativeServiceXml.class, componentClass);
				if (declarativeServiceXml == null) throw new NullPointerException("unable to convert component class '" + componentClass + "' to a declarative service");
				componentClass.getManifestFile().addDeclarativeServiceXml(declarativeServiceXml);
				componentClass.setDeclarativeServiceXml(declarativeServiceXml);
			}
		}
		
		if (resultingModelElement instanceof ManifestFile) {
			ManifestFile manifestFile = (ManifestFile) resultingModelElement;
			JavaTypeI componentContextType = ProvisionalTypes.getComponentcontext();
			JavaTypeI bundleContextType = vd.predef.org.osgi.framework.BundleContext.getType(); 
			manifestFile.addImportedPackage(componentContextType);
			manifestFile.addImportedPackage(bundleContextType);
			manifestFile.addImportedPackageAsString("org.slf4j", null);
		}
		
		// --- common way to add logging support, default is slf4j TODO make this configurable
		if (resultingModelElement instanceof JavaClass) {
			JavaClass javaClass = (JavaClass) resultingModelElement;
			
			JavaTypeI slf4jLoggerInterface = ProvisionalTypes.getLogger();
			JavaTypeI slf4jLoggerFactory = ProvisionalTypes.getLoggerfactory();
			
			JavaField loggerField = getJavaCreator().createFinalStaticPrivateField(slf4jLoggerInterface, "logger", javaClass);
			loggerField.setDefaultValue(slf4jLoggerFactory.getQualifiedName() + ".getLogger(" + javaClass.getName() + ".class)");
		}
		
		// TODO add component methods (activate(), deactivate(), ...)
	}

	protected JavaModelElementCreator getJavaCreator() {
		return getModelConverter().getJavaModelElementCreator();
	}
	
	/**
	 * TODO make this configurable, e.g. through a generator option
	 * @return
	 */
	protected RequiredExecutionEnironment getBundleRequiredExcecutionEnvironment() {
//		return RequiredExecutionEnironment.J2SE_1_8; TODO make this configurable through generator options
		return null;
	}
	
	/**
	 * @param version
	 * @return the bundle version for the given version object, the empty bundle version "0.0.0" in case version or something else is null
	 */
	protected org.osgi.framework.Version getBundleVersion(Version version) {
		org.osgi.framework.Version bundleVersion = null;
		if (version != null && version.getSemanticVersion() != null) {
		    bundleVersion = version.getSemanticVersion();
		}
		
		if (bundleVersion == null) {
			bundleVersion = org.osgi.framework.Version.emptyVersion;
		}
		
		return bundleVersion;
	}
}

