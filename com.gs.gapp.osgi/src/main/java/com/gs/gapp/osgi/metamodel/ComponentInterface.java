/**
 * 
 */
package com.gs.gapp.osgi.metamodel;

import com.gs.gapp.metamodel.java.JavaInterface;
import com.gs.gapp.metamodel.java.JavaModifier;
import com.gs.gapp.metamodel.java.JavaPackage;

/**
 * @author mmt
 *
 */
public class ComponentInterface extends JavaInterface {

	private static final long serialVersionUID = -8375572358368106188L;

	/**
	 * @param name
	 * @param aPackage
	 */
	public ComponentInterface(String name, JavaPackage aPackage) {
		super(name, JavaModifier.PUBLIC, aPackage);
	}
}
