package com.gs.gapp.osgi.generation.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.generation.xml.target.XMLDocumentTarget;
import com.gs.gapp.generation.xml.target.XMLDocumentTargetDocument;
import com.gs.gapp.generation.xml.writer.XMLDocumentWriter;
import com.gs.gapp.osgi.metamodel.DeclarativeServiceXml;

//@OneOff
public class DeclarativeServiceXmlTarget extends XMLDocumentTarget {

	@ModelElement
	private DeclarativeServiceXml declarativeServiceXml;
	
	@Override
	public URI getTargetURI() {
		try {
			return new URI(declarativeServiceXml.getFileName());
		} catch (URISyntaxException e) {
			throw createTargetException(e, this, declarativeServiceXml);
		}
	}
	
	public static class DeclarativeServiceXmlWriter extends XMLDocumentWriter {

		@ModelElement
		private DeclarativeServiceXml declarativeServiceXml;
		
		@Override
		public void transform(TargetSection ts) {
			if (XMLDocumentTargetDocument.CONTENT == ts) {
			    wNL( DeclarativeServiceXmlWriterSnippets.getContent(declarativeServiceXml) );
			}
		}
	}
}
