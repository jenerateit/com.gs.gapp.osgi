/**
 * 
 */
package com.gs.gapp.osgi.generation.target;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.generation.java.target.JavaClassTarget;
import com.gs.gapp.generation.java.writer.JavaMethodWriter;
import com.gs.gapp.osgi.metamodel.ComponentClass;

/**
 * @author mmt
 *
 */
public class ComponentClassTarget extends JavaClassTarget {

	@ModelElement
	private ComponentClass componentClass;
	
    public static class ComponentClassMethodWriter extends JavaMethodWriter {
		
		@ModelElement
		private ComponentClass componentClass;

		@Override
		protected JavaMethodWriter wOperationBody(TargetSection ts) {
			// TODO add anything here that's needed for all component classes
			return super.wOperationBody(ts);
		}
	}
}
