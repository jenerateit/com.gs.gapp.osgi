package com.gs.gapp.osgi.metamodel;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Version;
import org.osgi.framework.VersionRange;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.java.JavaPackage;
import com.gs.gapp.metamodel.java.JavaTypeI;

public class ManifestFile extends ModelElement {

	private static final long serialVersionUID = -8773552630189668346L;
	
	private final Set<DeclarativeServiceXml> declarativeServiceXmls = new LinkedHashSet<>();
	
	private final Map<JavaPackage, VersionRange> importedPackages = new LinkedHashMap<>();
	private final Map<String, VersionRange> importedPackagesAsStrings = new LinkedHashMap<>();
	private final Map<JavaPackage, VersionRange> exportedPackages = new LinkedHashMap<>();
	private final Map<String, VersionRange> requireBundles = new LinkedHashMap<>();
	
	private String bundleName;
    private String bundleSymbolicName;
    private boolean singleton = false;
    private boolean lazyBundleActivationPolicy = false;
    private Version bundleVersion;
    private String bundleVendor;
    private String fragmentHost;
    private String fragmentHostBundleVersion;
    private RequiredExecutionEnironment requiredExecutionEnvironment;
    private final Set<String> bundleClasspath = new LinkedHashSet<>();
    
    private final Map<String, String> customEntries = new LinkedHashMap<>();
    
    private final Set<NativeCode> nativeCodes = new LinkedHashSet<>();

	public ManifestFile(String name) {
		super(name);
	}

	public Map<JavaPackage, VersionRange> getExportedPackages() {
		return exportedPackages;
	}
	
	public void addExportedPackage(JavaTypeI javaType) {
		addExportedPackage(javaType.getJavaPackage());
	}
	
	public void addExportedPackage(JavaTypeI javaType, VersionRange version) {
		addExportedPackage(javaType.getJavaPackage(), version);
	}
	
	public void addExportedPackage(JavaPackage javaPackage) {
		addExportedPackage(javaPackage, null);
	}
	
	public void addExportedPackage(JavaPackage javaPackage, VersionRange version) {
		this.exportedPackages.put(javaPackage, version);
	}

	public Map<JavaPackage, VersionRange> getImportedPackages() {
		return importedPackages;
	}
	
	public void addImportedPackage(JavaTypeI javaType) {
		addImportedPackage(javaType.getJavaPackage());
	}
	
	public void addImportedPackage(JavaTypeI javaType, VersionRange version) {
		addImportedPackage(javaType.getJavaPackage(), version);
	}
	
	public void addImportedPackage(JavaPackage javaPackage) {
		addImportedPackage(javaPackage, null);
	}
	
	public void addImportedPackage(JavaPackage javaPackage, VersionRange version) {
		this.importedPackages.put(javaPackage, version);
	}
	
	public Map<String, VersionRange> getRequireBundles() {
		return requireBundles;
	}
	
	public void addRequireBundle(String bundle) {
		addRequireBundle(bundle, null);
	}
	
	public void addRequireBundle(String bundle, VersionRange version) {
		this.requireBundles.put(bundle, version);
	}

	public String getBundleName() {
		return bundleName;
	}

	public void setBundleName(String bundleName) {
		this.bundleName = bundleName;
	}
	
	public String getFragmentHost() {
		return fragmentHost;
	}
	
	public String getFragmentHostBundleVersion() {
		return fragmentHostBundleVersion;
	}
	
	public void setFragmentHost(String fragmentHost, String bundleVersion) {
		this.fragmentHost = fragmentHost;
		this.fragmentHostBundleVersion = bundleVersion;
	}

	public String getBundleSymbolicName() {
		return bundleSymbolicName;
	}

	public void setBundleSymbolicName(String bundleSymbolicName) {
		this.bundleSymbolicName = bundleSymbolicName;
	}

	public Set<String> getBundleClasspath() {
		return bundleClasspath;
	}
	
	public boolean addBundleClasspath(String classpathFragment) {
		return this.bundleClasspath.add(classpathFragment);
	}
	
	/**
	 * @deprecated no longer used in generation group
	 * @return
	 */
	@Deprecated
	public String getBundleClasspathAsString() {
		StringBuilder result = new StringBuilder("");
		String comma = "";
		for (String classpathEntry : bundleClasspath) {
			result.append(comma).append(classpathEntry);
			comma = ",";
		}
		
		return result.toString();
	}

	public String getBundleVendor() {
		return bundleVendor;
	}

	public void setBundleVendor(String bundleVendor) {
		this.bundleVendor = bundleVendor;
	}

	public Set<DeclarativeServiceXml> getDeclarativeServiceXmls() {
		return declarativeServiceXmls;
	}
	
	public boolean addDeclarativeServiceXml(DeclarativeServiceXml declarativeServiceXml) {
		if (declarativeServiceXml == null) throw new NullPointerException("parameter 'declarativeServiceXml' must not be null");
		return this.declarativeServiceXmls.add(declarativeServiceXml);
	}

	/**
	 * @deprecated no longer used in generation group
	 * @return
	 */
	@Deprecated
	public String getDeclarativeServiceXmlsAsCommaSeparatedList() {
		String comma = "";
		StringBuilder result = new StringBuilder();
		for (DeclarativeServiceXml declarativeServiceXml : this.declarativeServiceXmls) {
			result.append(comma).append(declarativeServiceXml.getFileName());
			comma = ",\n ";
		}
		
		if (result.length() > 0) {
		    return "Service-Component: " + result.toString();
		}
		
		return "";
	}

	/**
	 * @deprecated no longer used in generation group
	 * @return
	 */
	@Deprecated
	public String getImportedPackagesAsCommaSeparatedList() {
		String importedPackagesAsCommaSeparatedList = getJavaPackagesAsCommaSeparatedList(importedPackages);
		String importedPackagesAsCommaSeparatedList2 = getJavaPackageNamesAsCommaSeparatedList(importedPackagesAsStrings);
		String totalList = "";
		if (importedPackagesAsCommaSeparatedList != null && importedPackagesAsCommaSeparatedList.length() > 0 &&
				importedPackagesAsCommaSeparatedList2 != null && importedPackagesAsCommaSeparatedList2.length() > 0) {
			totalList = importedPackagesAsCommaSeparatedList + ",\n " + importedPackagesAsCommaSeparatedList2;
		} else if (importedPackagesAsCommaSeparatedList != null && importedPackagesAsCommaSeparatedList.length() > 0) {
			totalList = importedPackagesAsCommaSeparatedList;
		} else if (importedPackagesAsCommaSeparatedList2 != null && importedPackagesAsCommaSeparatedList2.length() > 0) {
			totalList = importedPackagesAsCommaSeparatedList2;
		}
				
		if (totalList != null && totalList.length() > 0) {
		    return "Import-Package: " + totalList;
		}
		
		return "";
	}

	/**
	 * @deprecated no longer used in generation group
	 * @return
	 */
	@Deprecated
	public String getExportedPackagesAsCommaSeparatedList() {
		String exportedPackagesAsCommaSeparatedList = getJavaPackagesAsCommaSeparatedList(exportedPackages);
		if (exportedPackagesAsCommaSeparatedList != null && exportedPackagesAsCommaSeparatedList.length() > 0) {
		    return "Export-Package: " + exportedPackagesAsCommaSeparatedList;
		}
		
		return "";
	}

	/**
	 * @deprecated no longer used in generation group
	 * @return
	 */
	@Deprecated
	private String getJavaPackagesAsCommaSeparatedList(Map<JavaPackage,VersionRange> javaPackagesMap) {
		String comma = "";
		String indent = "";
		StringBuilder result = new StringBuilder();
		for (JavaPackage javaPackage : javaPackagesMap.keySet()) {
			VersionRange versionRange = javaPackagesMap.get(javaPackage);
			String versionAsString = versionRange != null ? "version=\"" + versionRange.toString() + "\"" : "";
			result.append(comma).append(indent)
			       .append(javaPackage.getQualifiedName("."))
			       .append(versionRange != null && versionAsString != null && versionAsString.length() > 0 ? ";" : "")
			       .append(versionAsString);
			comma = ",\n";
			indent = " ";
		}
		
		return result.toString();
	}


	/**
	 * @deprecated no longer used in generation group
	 * @return
	 */
	@Deprecated
	private String getJavaPackageNamesAsCommaSeparatedList(Map<String,VersionRange> javaPackagesMap) {
		String comma = "";
		String indent = "";
		StringBuilder result = new StringBuilder();
		for (String javaPackageName : javaPackagesMap.keySet()) {
			VersionRange versionRange = javaPackagesMap.get(javaPackageName);
			String versionAsString = versionRange != null ? "version=\"" + versionRange.toString() + "\"" : "";
			result.append(comma).append(indent)
			       .append(javaPackageName)
			       .append(versionRange != null && versionAsString != null && versionAsString.length() > 0 ? ";" : "")
			       .append(versionAsString);
			comma = ",\n";
			indent = " ";
		}
		
		return result.toString();
	}

	public Version getBundleVersion() {
		return bundleVersion;
	}

	public void setBundleVersion(Version bundleVersion) {
		this.bundleVersion = bundleVersion;
	}
	
	public void setBundleVersion(String bundleVersionAsString) {
		try {
		    setBundleVersion(Version.parseVersion(bundleVersionAsString));
		} catch (IllegalArgumentException ex) {
			throw new RuntimeException("the version string '" + bundleVersionAsString + "' is not properly formated to be parsed into an instance of org.osgi.framework.Version");
		}
	}

	public Map<String, String> getCustomEntries() {
		return customEntries;
	}
	
	public void putCustomEntry(String key, String value) {
		this.customEntries.put(key, value);
	}
	
	/**
	 * @deprecated no longer used in generation group
	 * @return
	 */
	@Deprecated
	public String getCustomEntriesAsCommaSeparatedList() {
		String newline = "";
		StringBuilder result = new StringBuilder();
		for (String key : customEntries.keySet()) {
			String value = customEntries.get(key);
			result.append(newline)
			       .append(key)
			       .append(": ")
			       .append(value);
			newline = "\n";
		}
		
		return result.toString();
	}
	
	/**
	 * @return
	 */
	public String getNativeCodesAsCommaSeparatedList() {
		StringBuilder sb = new StringBuilder();
		if (nativeCodes.size() > 0) {
		
			String commaAndNewline = "";
			sb.append("Bundle-NativeCode:");
			for (NativeCode nativeCode : nativeCodes) {
				sb.append(commaAndNewline).append(" ");
				
				String semicolon = "";
				for (String libraryName : nativeCode.getLibraryNames()) {
					sb.append(semicolon).append(libraryName);
					semicolon = "; ";
				}
				
				for (String osName : nativeCode.getOsNames()) {
					sb.append(semicolon).append("osname=").append(osName);
				}
                for (String processorName : nativeCode.getProcessorNames()) {
                	sb.append(semicolon).append("processor=").append(processorName);
				}

                commaAndNewline = ",\n";
			}
		}
		
		return sb.toString();
	}
	
	public RequiredExecutionEnironment getRequiredExecutionEnvironment() {
		return requiredExecutionEnvironment;
	}

	public void setRequiredExecutionEnvironment(RequiredExecutionEnironment requiredExecutionEnvironment) {
		this.requiredExecutionEnvironment = requiredExecutionEnvironment;
	}

	public Set<NativeCode> getNativeCodes() {
		return nativeCodes;
	}
	
	public boolean addNativeCode(NativeCode nativeCode) {
		return this.nativeCodes.add(nativeCode);
	}

	public boolean isSingleton() {
		return singleton;
	}

	public void setSingleton(boolean singleton) {
		this.singleton = singleton;
	}

	public boolean isLazyBundleActivationPolicy() {
		return lazyBundleActivationPolicy;
	}

	public void setLazyBundleActivationPolicy(boolean lazyBundleActivationPolicy) {
		this.lazyBundleActivationPolicy = lazyBundleActivationPolicy;
	}

	public Map<String, VersionRange> getImportedPackagesAsStrings() {
		return importedPackagesAsStrings;
	}
	
	public void addImportedPackageAsString(String packageName, VersionRange versionRange) {
		this.importedPackagesAsStrings.put(packageName, versionRange);
	}

	/**
	 * @author mmt
	 *
	 */
	public static enum RequiredExecutionEnironment {
	     J2SE_1_2 ("J2SE-1.2"),
	     J2SE_1_3 ("J2SE-1.3"),
	     J2SE_1_4 ("J2SE-1.4"),
	     J2SE_1_5 ("J2SE-1.5"),
	     J2SE_1_6 ("JavaSE-1.6"),
	     J2SE_1_7 ("JavaSE-1.7"),
	     J2SE_1_8 ("JavaSE-1.8"),
	     J2SE_1_9 ("JavaSE-1.9"),
	     OSGI_MINIMUM ("OSGi/Minimum-1.1"),
	     CDC_1_1_FOUNDATION ("CDC-1.1/Foundation-1.1"),
	     CDC_1_1_PERSONAL_BASIS ("CDC-1.1/PersonalBasis-1.1"),
	     CDC_1_1_PERSONAL_JAVA ("CDC-1.1/PersonalJava-1.1"),
         ;
		
		private String name;
		
		private RequiredExecutionEnironment(String name) {
			this.setName(name);
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}
	
	/**
	 * @author mmt
	 *
	 */
	public static class NativeCode {
		
		private final Set<String> libraryNames = new LinkedHashSet<>();
		private final Set<String> osNames = new LinkedHashSet<>();
		private final Set<String> processorNames = new LinkedHashSet<>();
		
		/**
		 * @param libraryNames
		 */
		public NativeCode(String ...libraryNames) {
			super();
			if (libraryNames != null) this.libraryNames.addAll(Arrays.asList(libraryNames));
		}

		public Set<String> getLibraryNames() {
			return libraryNames;
		}

		public Set<String> getOsNames() {
			return osNames;
		}
		
		public boolean addOsName(String osName) {
			return this.osNames.add(osName);
		}

		public Set<String> getProcessorNames() {
			return processorNames;
		}
		
		public boolean addProcessorName(String processorName) {
			return this.processorNames.add(processorName);
		}
	}
}
