/**
 * 
 */
package com.gs.gapp.osgi.metamodel;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import com.gs.gapp.metamodel.java.JavaClass;
import com.gs.gapp.metamodel.java.JavaField;
import com.gs.gapp.metamodel.java.JavaInterface;
import com.gs.gapp.metamodel.java.JavaModifier;
import com.gs.gapp.metamodel.java.JavaPackage;

/**
 * @author mmt
 *
 */
public class ComponentClass extends JavaClass {

	private static final long serialVersionUID = -1704622159651917605L;
	
	private final Set<JavaInterface> componentInterfaces = new LinkedHashSet<>();
	private ManifestFile manifestFile;
	private DeclarativeServiceXml declarativeServiceXml;
	
	private JavaField propertiesField;

	/**
	 * @param name
	 * @param aPackage
	 */
	public ComponentClass(String name, JavaPackage aPackage) {
		super(name, JavaModifier.PUBLIC, aPackage);
	}
	
	protected ComponentClass(String name, int modifier, JavaPackage aPackage) {
		super(name, modifier, aPackage);
	}

	public Set<JavaInterface> getComponentInterfaces() {
		return componentInterfaces;
	}
	
	public boolean addComponentInterface(JavaInterface componentInterface) {
		boolean result = this.componentInterfaces.add(componentInterface);
		return result;
	}

	public ManifestFile getManifestFile() {
		return manifestFile;
	}

	public void setManifestFile(ManifestFile manifestFile) {
		this.manifestFile = manifestFile;
	}

	public DeclarativeServiceXml getDeclarativeServiceXml() {
		return declarativeServiceXml;
	}

	public void setDeclarativeServiceXml(DeclarativeServiceXml declarativeServiceXml) {
		this.declarativeServiceXml = declarativeServiceXml;
	}
	
    public JavaField getPropertiesField() {
		return propertiesField;
	}

	public void setPropertiesField(JavaField propertiesField) {
		this.propertiesField = propertiesField;
	}

	public static enum ComponentClassFieldEnum {
		
		PROPERTIES ("properties", "holds configuration properties for the bundle or service"),
		;

		private final String name;
		private final String documentation;
		
		private ComponentClassFieldEnum(String name, String documentation) {
			this.name = name;
			this.documentation = documentation;
		}

		public String getName() {
			return name;
		}

		public String getDocumentation() {
			return documentation;
		}
	}
	
	public enum ComponentClassMethodEnum {
		
		ACTIVATE ("activate", "called when OSGi framework activates the component"),
		MODIFIED ("modified", "called when OSGi framework modifies the component"),
		DEACTIVATE ("deactivate", "called when OSGi framework deactivates the component"),
		;
		
		private static final Map<String,ComponentClassMethodEnum> map = new HashMap<>();
		
		static {
			for (ComponentClassMethodEnum enumEntry : ComponentClassMethodEnum.values()) {
				map.put(enumEntry.getName().toLowerCase(), enumEntry);
			}
		}
		
		public static ComponentClassMethodEnum forName(String name) {
			return map.get(name.toLowerCase());
		}

		private final String name;
		private final String documentation;
		
		private ComponentClassMethodEnum(String name, String documentation) {
			this.name = name;
			this.documentation = documentation;
		}

		public String getName() {
			return name;
		}

		public String getDocumentation() {
			return documentation;
		}
	}
}
