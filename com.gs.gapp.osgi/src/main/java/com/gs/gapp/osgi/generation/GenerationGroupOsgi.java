/*
 * Copyright (c) 2007 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.osgi.generation;

import java.util.Set;

import org.jenerateit.generationgroup.WriterLocatorI;
import org.jenerateit.target.TargetI;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.basic.AbstractMetatypeFilter;
import com.gs.gapp.generation.basic.AbstractTargetSet;
import com.gs.gapp.generation.basic.WriterMapper;
import com.gs.gapp.generation.java.GenerationGroupJava;
import com.gs.gapp.generation.java.WriterLocatorJava;
import com.gs.gapp.generation.java.writer.JavaClassWriter;
import com.gs.gapp.generation.java.writer.JavaInterfaceWriter;
import com.gs.gapp.osgi.generation.target.BuildPropertiesFileTarget;
import com.gs.gapp.osgi.generation.target.BuildPropertiesFileTarget.BuildPropertiesFileWriter;
import com.gs.gapp.osgi.generation.target.ComponentClassTarget;
import com.gs.gapp.osgi.generation.target.ComponentInterfaceTarget;
import com.gs.gapp.osgi.generation.target.DeclarativeServiceXmlTarget;
import com.gs.gapp.osgi.generation.target.ManifestFileTarget;
import com.gs.gapp.osgi.generation.target.MetatypeXmlTarget;
import com.gs.gapp.osgi.metamodel.BuildPropertiesFile;
import com.gs.gapp.osgi.metamodel.ComponentClass;
import com.gs.gapp.osgi.metamodel.ComponentInterface;
import com.gs.gapp.osgi.metamodel.DeclarativeServiceXml;
import com.gs.gapp.osgi.metamodel.ManifestFile;
import com.gs.gapp.osgi.metamodel.MetatypeXml;

/**
 * gApp Generation Group for Device Service for Java
 *
 * @author mmt
 *
 */
public class GenerationGroupOsgi extends GenerationGroupJava {

	private final WriterLocatorI writerLocator;
	

	/**
	 *
	 */
	public GenerationGroupOsgi() {
		super();
		addTargetClasses(new TargetSetOsgi());
		writerLocator = new WriterLocatorOsgi(getAllTargets());
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.java.GenerationGroupJava#getWriterLocator()
	 */
	@Override
	public WriterLocatorI getWriterLocator() {
		return writerLocator;
	}
	
	/**
	 * @author mmt
	 *
	 */
	public class TargetSetOsgi extends AbstractTargetSet {

		private static final long serialVersionUID = -1340558378483183700L;

		public TargetSetOsgi() {
			
			add(BuildPropertiesFileTarget.class);
			add(ComponentClassTarget.class);
			add(ComponentInterfaceTarget.class);
			add(DeclarativeServiceXmlTarget.class);
			add(ManifestFileTarget.class);
			add(MetatypeXmlTarget.class);
		}
	}
	
	
	/**
	 * @author mmt
	 *
	 */
	public class WriterLocatorOsgi extends WriterLocatorJava {

		private final GenerationGroupOsgiMetatypeFilter generationGroupMetatypeFilter = new GenerationGroupOsgiMetatypeFilter();

		public WriterLocatorOsgi(Set<Class<? extends TargetI<?>>> targetClasses) {
			super(targetClasses);
			
			addWriterMapperForGenerationDecision( new WriterMapper(BuildPropertiesFile.class, BuildPropertiesFileTarget.class, BuildPropertiesFileWriter.class) );
			addWriterMapperForGenerationDecision( new WriterMapper(ComponentClass.class, ComponentClassTarget.class, JavaClassWriter.class) );
			addWriterMapperForGenerationDecision( new WriterMapper(ComponentInterface.class, ComponentInterfaceTarget.class, JavaInterfaceWriter.class) );
			addWriterMapperForGenerationDecision( new WriterMapper(DeclarativeServiceXml.class, DeclarativeServiceXmlTarget.class, DeclarativeServiceXmlTarget.DeclarativeServiceXmlWriter.class) );
			addWriterMapperForGenerationDecision( new WriterMapper(ManifestFile.class, ManifestFileTarget.class, ManifestFileTarget.ManifestFileWriter.class) );
			addWriterMapperForGenerationDecision( new WriterMapper(MetatypeXml.class, MetatypeXmlTarget.class, MetatypeXmlTarget.MetatypeXmlWriter.class) );
		}

		/* (non-Javadoc)
		 * @see com.gs.gapp.generation.java.WriterLocatorJava#getWriterClass(java.io.Serializable, java.lang.Class)
		 */
		@Override
		public Class<? extends WriterI> getWriterClass(Object element,
				Class<? extends TargetI<?>> targetClass) {

			if (generationGroupMetatypeFilter.isTargetGeneratedForMetaType(element.getClass()) == false) {
				return null;
			}

			Class<? extends WriterI> writerClass = super.getWriterClass(element, targetClass);
			return writerClass;
		}
		
		/**
		 * @author mmt
		 *
		 */
		public class GenerationGroupOsgiMetatypeFilter extends AbstractMetatypeFilter {

			{
				addMetaTypeWithTargetGeneration(BuildPropertiesFile.class);
				addMetaTypeWithTargetGeneration(ComponentClass.class);
				addMetaTypeWithTargetGeneration(ComponentInterface.class);
				addMetaTypeWithTargetGeneration(DeclarativeServiceXml.class);
				addMetaTypeWithTargetGeneration(ManifestFile.class);
				addMetaTypeWithTargetGeneration(MetatypeXml.class);
			}
		}
	}
}
