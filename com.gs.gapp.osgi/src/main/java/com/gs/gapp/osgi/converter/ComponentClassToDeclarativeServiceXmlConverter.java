/**
 * 
 */
package com.gs.gapp.osgi.converter;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.osgi.metamodel.ComponentClass;
import com.gs.gapp.osgi.metamodel.DeclarativeServiceXml;

/**
 * @author mmt
 *
 */
public class ComponentClassToDeclarativeServiceXmlConverter<S extends ComponentClass, T extends DeclarativeServiceXml> extends AbstractBasicToOsgiElementConverter<S, T> {

	public ComponentClassToDeclarativeServiceXmlConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}

	@Override
	protected T onCreateModelElement(S componentClass, ModelElementI previousResultingModelElement) {
		
		@SuppressWarnings("unchecked")
		T result = (T) new DeclarativeServiceXml(componentClass.getQualifiedName());
		result.setComponentClass(componentClass);
		return result;
	}
	
	

	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		return super.isResponsibleFor(originalModelElement, previousResultingModelElement, false);
	}

	@Override
	protected void onConvert(S componentClass, T declarativeServiceXml) {
		super.onConvert(componentClass, declarativeServiceXml);
		
		// TODO add more things here
		
	}
}
