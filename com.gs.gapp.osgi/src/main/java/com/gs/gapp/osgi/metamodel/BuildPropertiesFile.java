/**
 * 
 */
package com.gs.gapp.osgi.metamodel;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.java.PropertiesFile;

/**
 * @author mmt
 *
 */
public class BuildPropertiesFile extends PropertiesFile {

	private static final long serialVersionUID = 8085641786584388147L;
	
	private ManifestFile manifestFile;
	
	private final Set<String> binIncludes = new LinkedHashSet<>();
	
	private final Set<String> sourcePaths = new LinkedHashSet<>();
	
	private String output;

	public BuildPropertiesFile() {
		super("build.properties");
	}

	public ManifestFile getManifestFile() {
		return manifestFile;
	}

	public void setManifestFile(ManifestFile manifestFile) {
		this.manifestFile = manifestFile;
	}

	public Set<String> getSourcePaths() {
		return sourcePaths;
	}
	
	public boolean addSourcePath(String sourcePath) {
		return this.sourcePaths.add(sourcePath);
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public Set<String> getBinIncludes() {
		return binIncludes;
	}
	
	public boolean addBinInclude(String binInclude) {
		return this.binIncludes.add(binInclude);
	}
}
