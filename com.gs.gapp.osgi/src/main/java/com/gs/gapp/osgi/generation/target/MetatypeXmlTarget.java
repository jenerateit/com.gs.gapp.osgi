package com.gs.gapp.osgi.generation.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.generation.xml.target.XMLDocumentTarget;
import com.gs.gapp.generation.xml.target.XMLDocumentTargetDocument;
import com.gs.gapp.generation.xml.writer.XMLDocumentWriter;
import com.gs.gapp.osgi.metamodel.MetatypeXml;

//@OneOff
public class MetatypeXmlTarget extends XMLDocumentTarget {

	@ModelElement
	private MetatypeXml metatypeXml;
	
	@Override
	public URI getTargetURI() {
		try {
			return new URI(metatypeXml.getFileName());
		} catch (URISyntaxException e) {
			throw createTargetException(e, this, metatypeXml);
		}
	}
	
	public static class MetatypeXmlWriter extends XMLDocumentWriter {

		@ModelElement
		private MetatypeXml metatypeXml;
		
		@Override
		public void transform(TargetSection ts) {
			if (XMLDocumentTargetDocument.CONTENT == ts) {
			    wNL( MetatypeXmlWriterSnippets.getContent(metatypeXml) );
			}
		}
	}
}
