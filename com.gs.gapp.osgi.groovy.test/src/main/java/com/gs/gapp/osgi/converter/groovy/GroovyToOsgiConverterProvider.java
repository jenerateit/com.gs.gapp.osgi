/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */
package com.gs.gapp.osgi.converter.groovy;

import org.jenerateit.modelconverter.ModelConverterI;
import org.jenerateit.modelconverter.ModelConverterProviderI;

/**
 * @author mmt
 *
 */
public class GroovyToOsgiConverterProvider implements ModelConverterProviderI {
	
	/**
	 * 
	 */
	public GroovyToOsgiConverterProvider() {
		super();
	}
	
	@Override
	public ModelConverterI getModelConverter() {
		return new GroovyToOsgiConverter();
	}
}
