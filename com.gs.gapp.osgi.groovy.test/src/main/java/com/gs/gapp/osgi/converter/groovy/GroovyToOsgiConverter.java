package com.gs.gapp.osgi.converter.groovy;

import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.vd.converter.groovy.any.GroovyToAnyConverter;

/**
 * This converter is only needed for testing purposes, in order to
 * be able to define a generator that does not need any specific input
 * format (other than Groovy scripts).
 * 
 * @author mmt
 *
 */
public class GroovyToOsgiConverter extends GroovyToAnyConverter {
	
	/**
	 * @param modelElementCache
	 */
	public GroovyToOsgiConverter() {
		super(new ModelElementCache());
	}
}
